import { League } from "interfaces/leagues.interface";
import leagueModel from "../models/league.model";

class LeagueService {
  public leagues = leagueModel;

  public async findAllLeagues(): Promise<League[]> {
    const leagues = await this.leagues.find().populate('teams');
    return leagues;
  }

  public async findLeagueById(id: any): Promise<League> {
    const league = await this.leagues.findById(id).populate('teams');
    return league;
  }

  public async autocomplete (q: any ): Promise<League[]> {
    const query = {
      "$or": [{"name": {$regex: q, "$options": "i"}}]
    };
    const leagues = await this.leagues.find(query).limit(10).select('name');
    return leagues;
  }
}

export default LeagueService;
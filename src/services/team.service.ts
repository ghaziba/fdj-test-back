import { Team } from "interfaces/teams.interface";
import teamModel from "../models/team.model";

class TeamService {
    public teams = teamModel;
  
    public async findAllteams(): Promise<Team[]> {
      const teams = await this.teams.find().populate('players');
      return teams;
    }

    public async findTeamById(id: any): Promise<Team> {
      const team = await this.teams.findById(id).populate('players');
      return team;
    }
  }
  
  export default TeamService;
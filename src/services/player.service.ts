import { Player } from "interfaces/players.interface";
import playerModel from "../models/player.model";

class PlayerService {
    public players = playerModel;
  
    public async findAllplayers(): Promise<Player[]> {
      const players = await this.players.find();
      return players;
    }

    public async findPlayerById(id: any): Promise<Player> {
      const player = await this.players.findById(id);
      return player;
    }

  }
  
  export default PlayerService;
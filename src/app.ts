import * as cors from 'cors';
import * as express from 'express';
import * as helmet from 'helmet';
import * as hpp from 'hpp';
import * as mongoose from 'mongoose';
import * as logger from 'morgan';
import * as redis from 'redis';
import Routes from './interfaces/routes.interface';
import errorMiddleware from './middlewares/error.middleware';
import swaggerIgnite from './utils/swaggerIgnite';

class App {
  public app: express.Application;
  public port: (string | number);
  public env: boolean;

  constructor(routes: Routes[]) {
    this.app = express();
    this.port = process.env.PORT || 3033;
    this.env = process.env.NODE_ENV === 'production' ? true : false;

    this._connectToDatabase();
    this._initializeMiddlewares();
    this.initSwaggerDocs();
    this._initializeRoutes(routes);   
    this._initializeErrorHandling();
  }

  public listen() {
    this.app.listen(this.port, () => {
      console.log(`App listening on the port ${this.port}`);
      logger(`App listening on the port ${this.port}`);
    });
  }

  public getServer() {
    return this.app;
  }

  public initSwaggerDocs() {
    swaggerIgnite(this.app);
  }

  private _initializeMiddlewares() {
    if (this.env) {
      this.app.use(hpp());
      this.app.use(helmet());
      this.app.use(logger('combined'));
      this.app.use(cors({ origin: `${process.env.APP_PATH}:${this.port}`, credentials: true }));
    } else {
      this.app.use(logger('dev'));
      this.app.use(cors({ origin: true, credentials: true }));
    }

    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: true }));
  }

  private _initializeRoutes(routes: Routes[]) {
    routes.forEach((route) => {
      this.app.use('/', route.router);
    });
  }

  private _initializeErrorHandling() {
    this.app.use(errorMiddleware);
  }

  private _connectToDatabase() {
    const { MONGO_USER, MONGO_PASSWORD, MONGO_PATH, MONGO_CONNECTION_VERB } = process.env;
    const options = {
        useCreateIndex: true,
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
      };
    
      if (this.env) {
        // production database
      } else {
        mongoose.connect(MONGO_CONNECTION_VERB+'://'+MONGO_USER+':'+MONGO_PASSWORD+MONGO_PATH, { ...options });
      }
  }

  private _connectToRedisCache () {
    const client = redis.createClient();
    client.connect();
  }
}

export default App;

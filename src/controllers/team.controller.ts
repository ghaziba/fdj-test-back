import { NextFunction, Request, Response } from "express";
import { Team } from "interfaces/teams.interface";
import errorMiddleware from "../middlewares/error.middleware";
import teamService from "../services/team.service";

class TeamController {
    public teamService = new teamService();
  
    public getTeams = async (req: Request, res: Response, next: NextFunction) => {
      try {
        const findAllTeamsData: Team[] = await this.teamService.findAllteams();
        res.status(200).json({ data: findAllTeamsData, message: 'findAll' });
      } catch (error) {
        next(errorMiddleware(error,req,res));
      }
    }

    public getTeamById = async (req: Request, res: Response, next: NextFunction) => {
      try {
        const teamId = req.params.id;
        const findTeamData: Team = await this.teamService.findTeamById(teamId);
        res.status(200).json({ data: findTeamData, message: 'findById' });
      } catch (error) {
        next(errorMiddleware(error,req,res));
      }
    }
}

export default TeamController;
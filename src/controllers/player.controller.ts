import { NextFunction, Request, Response } from "express";
import { Player } from "interfaces/players.interface";
import errorMiddleware from "../middlewares/error.middleware";
import playerService from "../services/player.service";

class PlayerController {
    public playerService = new playerService();
  
    public getPlayers = async (req: Request, res: Response, next: NextFunction) => {
      try {
        const findAllPlayersData: Player[] = await this.playerService.findAllplayers();
        res.status(200).json({ data: findAllPlayersData, message: 'findAll' });
      } catch (error) {
        next(errorMiddleware(error,req,res));
      }
    }

    public getPlayerById = async (req: Request, res: Response, next: NextFunction) => {
      try {
        const playerId = req.params.id;
        const findPlayerData: Player = await this.playerService.findPlayerById(playerId);
        res.status(200).json({ data: findPlayerData, message: 'findById' });
      } catch (error) {
        next(errorMiddleware(error,req,res));
      }
    }
}

export default PlayerController;
import { NextFunction, Request, Response } from "express";
import { League } from "interfaces/leagues.interface";
import errorMiddleware from "../middlewares/error.middleware";
import leagueService from "../services/league.service";

class LeagueController {
    public leagueService = new leagueService();
  
    public getLeagues = async (req: Request, res: Response, next: NextFunction) => {
      try {
        const findAllLeaguesData: League[] = await this.leagueService.findAllLeagues();
        res.status(200).json({ data: findAllLeaguesData, message: 'findAll' });
      } catch (error) {
        next(error);
      }
    }

    public getLeagueById = async (req: Request, res: Response, next: NextFunction) => {
      try {
        const leagueId = req.params.id;
        const findLeagueData: League = await this.leagueService.findLeagueById(leagueId);
        res.status(200).json({ data: findLeagueData, message: 'findById' });
      } catch (error) {
        next(errorMiddleware(error,req,res));
      }
    }

    public autocomplete = async (req: Request, res: Response, next: NextFunction) => {
      try {
        const query = req.query.query;
        const findLeaguesDataSearch: League[] = await this.leagueService.autocomplete(query);
        res.status(200).json({ data: findLeaguesDataSearch, message: 'search' });
      } catch (error) {
        next(errorMiddleware(error,req,res));
      }
    }
    
}

export default LeagueController;
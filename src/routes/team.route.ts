import TeamController from "../controllers/team.controller";
import { Router } from "express";
import Route from "interfaces/routes.interface";

class TeamRoute implements Route {
    public path = '/teams';
    public router = Router();
    public teamController = new TeamController();
  
    constructor() {
      this.initializeRoutes();
    }
  
    private initializeRoutes() {
      this.router.get(`${this.path}`, this.teamController.getTeams);
      this.router.get(`${this.path}/:id`, this.teamController.getTeamById);
    }
  }

export default TeamRoute;
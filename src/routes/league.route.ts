import LeagueController from "../controllers/league.controller";
import { Router } from "express";
import Route from "interfaces/routes.interface";
import cacheMiddleware from "../middlewares/cache.middleware";

class LeagueRoute implements Route {
    public path = '/leagues';
    public router = Router();
    public leagueController = new LeagueController();
  
    constructor() {
      this.initializeRoutes();
    }
  
    private initializeRoutes() {
      this.router.get(`${this.path}`, this.leagueController.getLeagues);
      this.router.get(`${this.path}/search`, this.leagueController.autocomplete);
      this.router.get(`${this.path}/:id`, this.leagueController.getLeagueById);
    }
  }

export default LeagueRoute;
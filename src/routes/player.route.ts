import PlayerController from "../controllers/player.controller";
import { Router } from "express";
import Route from "interfaces/routes.interface";

class PlayerRoute implements Route {
    public path = '/players';
    public router = Router();
    public playerController = new PlayerController();
  
    constructor() {
      this.initializeRoutes();
    }
  
    private initializeRoutes() {
      this.router.get(`${this.path}`, this.playerController.getPlayers);
      this.router.get(`${this.path}/:id`, this.playerController.getPlayerById);
    }
  }

export default PlayerRoute;
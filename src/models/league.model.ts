import { League } from 'interfaces/leagues.interface';
import * as mongoose from 'mongoose';
import teamModel from './team.model';


const leagueSchema = new mongoose.Schema({
    name: String,
    sports: String,
    teams: [
        {
            type: mongoose.Schema.Types.ObjectId,
            require: true,
            ref: teamModel
          }
    ]
});

const leagueModel = mongoose.model<League & mongoose.Document>('leagues', leagueSchema);

export default leagueModel;
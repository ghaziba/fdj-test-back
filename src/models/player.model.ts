import { Player } from 'interfaces/players.interface';
import * as mongoose from 'mongoose';


const playerSchema = new mongoose.Schema({
    name: String,
    position: String,
    thumbnail: String,
    signin: {
        amount: Number,
        currency: String
    },
    born: Date,
});

const playerModel = mongoose.model<Player & mongoose.Document>('players', playerSchema);

export default playerModel;
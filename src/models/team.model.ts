
import { Team } from 'interfaces/teams.interface';
import * as mongoose from 'mongoose';
import playerModel from './player.model';

const teamSchema = new mongoose.Schema({
    name: String,
    thumbnail: String,
    players: [
        {
            type: mongoose.Schema.Types.ObjectId,
            require: true,
            ref: playerModel
          }
    ]
});

const teamModel = mongoose.model<Team & mongoose.Document>('teams', teamSchema);

export default teamModel;
import { Signin } from "./signin.interface";

export interface Player {
     _id: string;
     name: string;
     position: string;
     thumbnail: string;
     signin : Signin;
     born :Date;
  }
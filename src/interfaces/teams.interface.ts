import { Player } from "./players.interface";
import { Signin } from "./signin.interface";

export interface Team {
    _id: string;
    name: string;
    players: Player [];
 }
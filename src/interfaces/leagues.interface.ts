import { Team } from "./teams.interface";

export interface League {
     _id: string;
     name: string;
     sports: string;
     teams: Team [];
  }
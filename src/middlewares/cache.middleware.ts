import { Request, Response, NextFunction } from 'express';
import * as redis from 'redis';

const portRedis = process.env.PORT_REDIS || '6379';

async function cacheMiddleware(req: Request, res: Response, next: NextFunction) {
  const rediClient = redis.createClient();
  const { id }  = req.params;
  const data = await rediClient.get(id)
    if (data !== null) {
      return res.json(JSON.parse(data));
    } else {
      return next();
    }
}

export default cacheMiddleware;
import 'dotenv/config';
import App from './app';
import IndexRoute from './routes/index.route';
import validateEnv from './utils/validateEnv';
import LeagueRoute from './routes/league.route';
import PlayerRoute from './routes/player.route';
import TeamRoute from './routes/team.route';

validateEnv();

const app = new App([
  new IndexRoute(),
  new LeagueRoute(),
  new TeamRoute(),
  new PlayerRoute()
]);

app.listen();

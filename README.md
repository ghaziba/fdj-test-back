## steps to follow

npm install

npm run dev

Swagger documentation can be found at `http://localhost:<port>/api-docs`

## screenshots

![IMAGE_DESCRIPTION](https://gitlab.com/ghaziba/fdj-test-back/-/raw/master/screenshots/fdj-test-back-2.png)
![IMAGE_DESCRIPTION](https://gitlab.com/ghaziba/fdj-test-back/-/raw/master/screenshots/fdj-back-1.png)